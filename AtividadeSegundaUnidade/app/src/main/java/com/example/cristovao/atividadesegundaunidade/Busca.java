package com.example.cristovao.atividadesegundaunidade;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Busca extends AppCompatActivity {
    EditText tvnome;
    Button Buscar;
    BancoDados myDb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busca);
        myDb = new BancoDados(this);
        tvnome=(EditText) findViewById(R.id.nome_buscar);
        Buscar = (Button) findViewById(R.id.Buscar);

        Buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contato cursor = resultado(tvnome.getText().toString());
                Intent i = new Intent(Busca.this,Discar.class);
                i.putExtra("id", cursor.id);
                i.putExtra("nome", cursor.nome);
                i.putExtra("fone", cursor.telefone);
                i.putExtra("email", cursor.email);
                startActivity(i);

            }
        });

    }
    public Contato resultado(String nome){
        Cursor res = myDb.getBusca(nome);
        Contato contato = new Contato(res.getString(0),res.getString(1),res.getString(2),res.getString(3));
        return contato;
    }

}
