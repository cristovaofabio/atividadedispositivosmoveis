package com.example.cristovao.atividadesegundaunidade;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Discar extends AppCompatActivity {
    TextView tvnome,tvfone,tvemail,tvid;
    BancoDados myDb;
    Button discar,excluir,atualizar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discar);
        myDb = new BancoDados(this);
        configurarComponentes();
    }

    private void configurarComponentes() {
        tvid=(TextView) findViewById(R.id.id_detalhe);
        tvnome=(TextView) findViewById(R.id.nome_detalhe);
        tvfone=(TextView) findViewById(R.id.telefone_detalhe);
        tvemail=(TextView) findViewById(R.id.emai_detalhe);

        discar = (Button) findViewById(R.id.telefonar);
        excluir = (Button) findViewById(R.id.excluir);
        atualizar = (Button) findViewById(R.id.atualizar);

        Intent i = getIntent();

        tvid.setText(i.getStringExtra("id"));
        tvnome.setText(i.getStringExtra("nome"));
        tvfone.setText(i.getStringExtra("fone"));
        tvemail.setText(i.getStringExtra("email"));

        discar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("tel:"+tvfone.getText());
                Intent i = new Intent(Intent.ACTION_DIAL,uri);
                startActivity(i);
            }
        });
        excluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer deletedRows = myDb.deleteData(tvfone.getText().toString());
                if(deletedRows > 0)
                    Toast.makeText(Discar.this,"Data Deleted",Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(Discar.this,"Data not Deleted",Toast.LENGTH_LONG).show();
            }
        });
        atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Discar.this,Atualizar.class);
                i.putExtra("id",tvid.getText().toString());
                i.putExtra("nome",tvnome.getText().toString());
                i.putExtra("fone",tvfone.getText().toString());
                i.putExtra("email",tvemail.getText().toString());
                startActivity(i);
            }
        });

    }


}
