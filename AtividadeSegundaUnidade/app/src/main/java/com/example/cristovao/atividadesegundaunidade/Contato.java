package com.example.cristovao.atividadesegundaunidade;

/**
 * Created by cristovao on 28/05/17.
 */

public class Contato {
    public String nome,telefone,email;
    public String id;
    public Contato(){
        super();
    }
    public Contato(String id,String nome,String telefone,String email){
        super();
        this.id=id;
        this.nome=nome;
        this.telefone=telefone;
        this.email=email;
    }
}
