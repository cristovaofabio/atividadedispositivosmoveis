package com.example.cristovao.atividadesegundaunidade;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.Editable;

/**
 * Created by cristovao on 27/05/17.
 */

public class BancoDados extends SQLiteOpenHelper{
    public static final String NOME_BANCO="Bank.db";
    public static final String NOME_TABELA="contats_table";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NOME";
    public static final String COL_3 = "TELEFONE";
    public static final String COL_4 = "EMAIL";

    public BancoDados(Context context) {
        super(context,NOME_BANCO,null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+NOME_TABELA+" (ID INTEGER PRIMARY KEY " +
                "AUTOINCREMENT, NOME TEXT, TELEFONE TEXT, " +
                "EMAIL TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+NOME_TABELA);
    }

    public boolean insertData(String nome, String telefone, String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,nome);
        contentValues.put(COL_3,telefone);
        contentValues.put(COL_4,email);
        long result = db.insert(NOME_TABELA,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+NOME_TABELA,null);
        return res;
    }
    public Cursor getBusca(String nome) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+NOME_TABELA+" where NOME = "+nome,null);
        return res;
    }

    public Integer deleteData (String telefone) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(NOME_TABELA, "TELEFONE = ?",new String[] {telefone});
    }
    public boolean updateData(String id,String name,String telefone,String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,name);
        contentValues.put(COL_3,telefone);
        contentValues.put(COL_4,email);
        db.update(NOME_TABELA, contentValues, "ID = ?", new String[]{id});
        return true;
    }
    public String getID(String telefone) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+NOME_TABELA+" where TELEFONE = "+telefone,null);
        return res.getString(0);
    }
    public boolean updateTwo(String telefone,String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_3,telefone);
        contentValues.put(COL_4,email);
        db.update(NOME_TABELA, contentValues, "TELEFONE = ?", new String[]{telefone});
        return true;
    }
    public boolean updateONE(String nome,String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,nome);
        contentValues.put(COL_4,email);
        db.update(NOME_TABELA, contentValues, "NOME = ?", new String[]{nome});
        return true;
    }
}
