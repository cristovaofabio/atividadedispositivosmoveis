package com.example.cristovao.atividadesegundaunidade;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CadastrarContato extends AppCompatActivity {
    BancoDados banco;
    EditText nome,telefone,email;
    Button cadastrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastrar_contato);
        banco = new BancoDados(this);
        nome = (EditText)findViewById(R.id.nome);
        telefone = (EditText)findViewById(R.id.telefone);
        email = (EditText)findViewById(R.id.email);
        cadastrar = (Button) findViewById(R.id.cadastrar);
        AdicionarDados();
    }

    private void AdicionarDados() {
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isInserted = banco.insertData(nome.getText().toString(),
                        telefone.getText().toString(),email.getText().toString());
                if (isInserted==true){
                    Toast.makeText(CadastrarContato.this,"Contato inserido",Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(CadastrarContato.this, "Contato não inserido", Toast.LENGTH_LONG).show();
                }
                }
        });
    }
}
