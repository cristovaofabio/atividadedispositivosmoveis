package com.example.cristovao.atividadesegundaunidade;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by cristovao on 28/05/17.
 */

public class ContatoAdapter extends ArrayAdapter<Contato>{
    Context myContext;
    int myLayoutResourceID;
    ArrayList<Contato> mydata=null;
    public ContatoAdapter(Context context,int layoutResourceID, ArrayList<Contato> data){
        super(context,layoutResourceID,data);
        this.myContext=context;
        this.myLayoutResourceID=layoutResourceID;
        this.mydata=data;
    }
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        ContatoHolder holder =null;

        if (row==null){
            LayoutInflater inflater =((Activity)myContext).getLayoutInflater();
            row = inflater.inflate(myLayoutResourceID,parent,false);
            holder = new ContatoHolder();
            holder.id = (TextView) row.findViewById(R.id.id_person);
            holder.nome = (TextView) row.findViewById(R.id.nome_person);
            holder.email = (TextView) row.findViewById(R.id.emai_person);
            holder.telefone = (TextView) row.findViewById(R.id.telefone_person);
            row.setTag(holder);
        }else {
            holder = (ContatoHolder) row.getTag();
        }

        Contato contato = mydata.get(position);
        holder.id.setText(contato.id);
        holder.nome.setText(contato.nome);
        holder.telefone.setText(contato.telefone);
        holder.email.setText(contato.email);

        return row;
    }
    static class ContatoHolder{
        TextView id,nome,telefone,email;
    }
}
