package com.example.cristovao.atividadesegundaunidade;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Atualizar extends AppCompatActivity {
    BancoDados myDb;
    EditText nome_update,telefone_update,email_update;
    Button btnviewUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atualizar);
        configurarComponentes();
    }

    private void configurarComponentes() {
        myDb = new BancoDados(this);
        nome_update = (EditText) findViewById(R.id.nome_update);
        telefone_update = (EditText) findViewById(R.id.telefone_update);
        email_update = (EditText) findViewById(R.id.email_update);
        btnviewUpdate = (Button) findViewById(R.id.salvar);

        Intent i = getIntent();
        nome_update.setText(i.getStringExtra("nome"));
        final String id;
        id = i.getStringExtra("id");
        telefone_update.setText(i.getStringExtra("fone"));
        email_update.setText(i.getStringExtra("email"));

        btnviewUpdate.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            boolean isUpdate = myDb.updateData(id,nome_update.getText().toString(),
                                    telefone_update.getText().toString(), email_update.getText().toString());
                            if(isUpdate == true)
                                Toast.makeText(Atualizar.this,"Data Update",Toast.LENGTH_LONG).show();

                    }
                }
        );
    }
}
