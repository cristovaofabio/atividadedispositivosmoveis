package com.example.cristovao.atividadesegundaunidade;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button NovoContato,Listar,buscar;
    BancoDados banco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        banco = new BancoDados(this);
        configurarBotes();
    }

    private void configurarBotes() {
        NovoContato = (Button) findViewById(R.id.novo_contato);
        Listar = (Button) findViewById(R.id.listar_contato);
        buscar = (Button) findViewById(R.id.procurar_contato);

        NovoContato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intencao = new Intent();
                intencao.setClass(MainActivity.this,CadastrarContato.class);
                startActivity(intencao);
            }
        });
        Listar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intencao = new Intent();
                intencao.setClass(MainActivity.this,Lista.class);
                startActivity(intencao);
            }
        });
        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intencao = new Intent();
                intencao.setClass(MainActivity.this,Busca.class);
                startActivity(intencao);
            }
        });
    }

}
