package com.example.cristovao.atividadesegundaunidade;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class Lista extends AppCompatActivity {
    BancoDados banco;
    ListView listView;
    TextView tvnome,tvfone,tvemail,tvid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        banco = new BancoDados(this);
        ArrayList<Contato> con = new ArrayList<Contato>();
        con=viewAll();
        ContatoAdapter adapter = new ContatoAdapter(this,R.layout.lista_personalizada,con);
        listView = (ListView) findViewById(R.id.desenho);
        View header = (View) getLayoutInflater().inflate(R.layout.list_header_row,null);
        listView.addHeaderView(header);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(Lista.this,Discar.class);
                tvid = (TextView)view.findViewById(R.id.id_person);
                tvnome = (TextView)view.findViewById(R.id.nome_person);
                tvfone = (TextView)view.findViewById(R.id.telefone_person);
                tvemail = (TextView)view.findViewById(R.id.emai_person);
                i.putExtra("id",tvid.getText().toString());
                i.putExtra("nome",tvnome.getText().toString());
                i.putExtra("fone",tvfone.getText().toString());
                i.putExtra("email",tvemail.getText().toString());
                startActivity(i);

            }
        });


    }
    public ArrayList<Contato> viewAll() {
        Cursor res = banco.getAllData();
        if(res.getCount() == 0) {
            showMessage("Error","Nothing found");
            return null;
        }
        ArrayList<Contato> con = new ArrayList<Contato>();
        while (res.moveToNext()) {
            Contato contato = new Contato(res.getString(0),res.getString(1),res.getString(2),res.getString(3));
            con.add(contato);
        }
        return con;
    }
    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
    static class ContatoHolder{
        TextView nome,telefone,email;
    }
}
